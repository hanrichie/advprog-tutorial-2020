package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me
                this.guild = guild;
        }

        //ToDo: Complete Me
        @Override
        public void update() {
                Quest quest = guild.getQuest();
//                if (quest.getType().equals("Delivery") || quest.getType().equals("Rumble") || quest.getType().equals("Escort")) {
//                        this.getQuests().add(quest);
//                }
                this.getQuests().add(quest);
        }
}
