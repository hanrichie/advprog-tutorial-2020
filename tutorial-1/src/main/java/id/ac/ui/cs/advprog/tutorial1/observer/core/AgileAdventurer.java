package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
        }
        //ToDo: Complete Me

        @Override
        public void update() {
//                Quest quest = guild.getQuest();
//                if (quest.getType().equals("Delivery") || quest.getType().equals("Rumble")) {
//                        this.getQuests().add(quest);
//                }
                String quest = this.guild.getQuestType();
                if (quest.equals("D") || quest.equals("R")) {
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
