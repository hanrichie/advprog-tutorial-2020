package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @InjectMocks
    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("Test get wish");
    }

    @Test
    public void whenMakeAWishShouldSetWish() {
        holyGrail.makeAWish("Test set wish");
        assertEquals("Test set wish", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void whenGetWishShouldReturnWish() {
        assertEquals("Test get wish", holyGrail.getHolyWish().getWish());
    }
}
