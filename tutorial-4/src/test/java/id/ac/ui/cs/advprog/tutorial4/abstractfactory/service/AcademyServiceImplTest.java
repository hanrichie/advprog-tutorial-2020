package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @BeforeEach
    public void setUp() {
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldReturnKnightAcademies() {
//        assertEquals(academyService.getKnightAcademies(), academyRepository.getKnightAcademies());
        assertTrue(academyService.getKnightAcademies() instanceof List<?>);
    }

    @Test
    public void whenProduceKnightIsCalledItShouldReturnKnight() {
        String academyName = "Lordran";
        String knightType = "majestic";
        academyService.produceKnight(academyName, knightType);
        assertNotNull(academyService.getKnight());
    }
}
