package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String name;
    private String role;
    private List<Member> childMember;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childMember = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if (this.role != "Master") {
            if (childMember.size() < 3) {
                childMember.add(member);
            }
        } else {
            childMember.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        childMember.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childMember;
    }
}
