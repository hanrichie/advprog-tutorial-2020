package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
    //TODO: Complete me

    public Shield() {
        super.weaponName = "Shield";
        super.weaponValue = 10;
        super.weaponDescription = "Heater Shield";
    }
}
