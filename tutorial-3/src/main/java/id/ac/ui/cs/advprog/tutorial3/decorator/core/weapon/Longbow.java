package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    //TODO: Complete me

    public Longbow() {
        super.weaponName = "Longbow";
        super.weaponValue = 15;
        super.weaponDescription = "Big Longbow";
    }
}
