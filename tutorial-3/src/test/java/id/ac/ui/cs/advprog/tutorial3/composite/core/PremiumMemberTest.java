package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Test", "Roletest");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Test", "Roletest");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child1 = new OrdinaryMember("First", "Roletest");
        member.addChildMember(child1);
        Member child2 = new OrdinaryMember("Second", "Roletest");
        member.addChildMember(child2);
        Member child3 = new OrdinaryMember("Third", "Roletest");
        member.addChildMember(child3);
        Member child4 = new OrdinaryMember("Fourth", "Roletest");
        member.addChildMember(child4);
        assertEquals(3, member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Mastertest", "Master");
        Member child1 = new OrdinaryMember("First", "Roletest");
        member.addChildMember(child1);
        Member child2 = new OrdinaryMember("Second", "Roletest");
        member.addChildMember(child2);
        Member child3 = new OrdinaryMember("Third", "Roletest");
        member.addChildMember(child3);
        Member child4 = new OrdinaryMember("Fourth", "Roletest");
        member.addChildMember(child4);
        assertEquals(4, master.getChildMembers().size());
    }

}
